import React from "react";
import { Link } from 'react-router-dom'
export class Header extends React.Component {
	render() {
		return (
			<nav className="navbar navbar-default">
					<div className="container-fluid">
						<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul className="nav navbar-nav">
								<li><Link to="/">Home</Link></li>
								<li><Link to="/additems">Add Users</Link></li>
							</ul>  
						</div>
					</div>
					</nav>
		);
	}
}
