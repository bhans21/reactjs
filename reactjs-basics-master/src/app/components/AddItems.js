import React from "react";
import axios from 'axios';

console.clear();

const Title = ({todoCount}) => {
  return (
    <div>
       <div>
          <h1>to-do ({todoCount})</h1>
       </div>
    </div>
  );
};

const UserAlert = ({alerts}) => {
	return(
		<div className={ `alert alert-${alerts.msgclass}` } role={alerts.msgclass} dangerouslySetInnerHTML={{__html: alerts.msg}} />		
	)
}

const TodoForm = ({addTodo, alerts}) => {
  // Input Tracker
	let input;
	// Return JSX
  return (
    <form onSubmit={(e) => {
        e.preventDefault();
        addTodo(input.value);
        input.value = '';
      }}>
			<UserAlert alerts={alerts}/>
			<div className="form-group">
				<label htmlFor="user-name">User Name</label>
				<input className="form-control" id="user-name" ref={node => {
					input = node;
				}} />
			</div>
			<button className="btn btn-primary" onClick={(e) => {
				e.preventDefault();
				input.value != '' ? addTodo(input.value) : true;
				input.value = '';
			}}>
			Add User
			</button>	 
      <br />
    </form>
  );
};

const Todo = ({todo, remove}) => {
  // Each Todo
  return (<li className="list-group-item">{todo.text} <a href="#" onClick={() => {remove(todo.id)}}>X</a></li>);
}

const TodoList = ({todos, remove}) => {
  // Map through the todos
  const todoNode = todos.map((todo) => {
    return (<Todo todo={todo} key={todo.id} remove={remove}/>)
  });
  return (<ul className="list-group" style={{marginTop:'30px'}}>{todoNode}</ul>);
}


export class AddItems extends React.Component {
	constructor(props){
		// Pass props to parent class
	    super(props);
	    // Set initial state
	    this.state = {
				data: [],
				alerts: {
					msgclass: "hide",
					msg: ""
				}
	    }
			this.apiUrl = '//57b1924b46b57d1100a3c3f8.mockapi.io/api/todos';			
	}

	 // Lifecycle method
	 componentDidMount(){
    // Make HTTP reques with Axios
    axios.get(this.apiUrl)
      .then((res) => {
        // Set state with result
        this.setState({data:res.data});
      });
  }

	// Add todo handler
  addTodo(val){
    // Assemble data
		const todo = {text: val}
		let dupData = false;
    // Update data
		// Filter through the todos
	  this.state.data.forEach((todoText) => {
			if(todoText.text == todo.text){
				dupData = true;
			}
		});

		if(!dupData){
			axios.post(this.apiUrl, todo)
				.then((res) => {
						this.state.data.push(res.data);
						this.setState({
							data: this.state.data,
							alerts: {
								msgclass: "success",
								msg: "Username <b>" + val + "</b>	got added!!" 
							}
						});
			});
		}else{
			this.setState({
				alerts: {
					msgclass: "danger",
					msg: "Username <b>" + val + "</b> already take, Please try different name." 
				}
			})
		}
	}

	// Handle remove
  handleRemove(id){
		let username;
    // Filter all todos except the one to be removed
    const remainder = this.state.data.filter((todo) => {
			if(todo.id === id) username = todo.text;
      if(todo.id !== id) return todo;
    });
    // Update state with filter
    axios.delete(this.apiUrl+'/'+id)
      .then((res) => {
        this.setState({
					data: remainder,
					alerts: {
						msgclass: "info",
						msg: "Username <b>" + username + "</b> removed." 
					}
				});      
      })
  }
	
	render(){
	    // Render JSX
	    return (
	      <div>
	        <Title todoCount={this.state.data.length}/>
					<TodoForm addTodo={this.addTodo.bind(this)} alerts={this.state.alerts}/>
					<TodoList 
						todos={this.state.data} 
						remove={this.handleRemove.bind(this)}
					/>
	      </div>
	    );
	}
}
