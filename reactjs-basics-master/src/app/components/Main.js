import React from "react";

export class Main extends React.Component {
	constructor(props){
		super();
		this.state = {
			currentAge : props.age,
			status: 0
		}
	}

	incrementMethod() {
		this.setState({
			currentAge: this.state.currentAge + 1,
			status: 1
		})
	}

	render() {
		return (
			<div className="page-header">
			  <h1>Example page header <small>Subtext for header</small> </h1>
				<span>{this.state.currentAge}</span>
				<hr/>
				<button onClick={() => this.incrementMethod()}>click me</button>
			</div>
		);
	}
}

