import React from "react";
import { render } from "react-dom";
import { BrowserRouter as Router, Route } from 'react-router-dom'

import { Header } from "./components/header";
import { Main } from "./components/main";
import { AddItems } from "./components/additems";

class App extends React.Component {
	render() {
		return ( 
			<Router>
				<div className="container">					 
					<Header />	
					<div id="main">			
						<Route exact path="/" component={Main}/>
						<Route path="/additems" component={AddItems}/>
					</div>	
				</div>
			</Router>
		);
	}
}

render(<App/>, document.getElementById('app'));